import itertools
import csv
from multiprocessing import Manager, Process, Pool,Queue
from queue import Empty


class _Legion(object):

    def __init__(self, mproc_number, queue, consumer):
        self._worker = Process(target=consumer, args=(mproc_number, queue,))
        self._worker.start()

    def await_until_culminate(self):
        self._worker.join()

    @classmethod
    def engage(cls, fpath, lsize, producer, consumer, nproc=3):
        queue = Queue()
        supplier = Process(target=producer, args=(fpath, queue, lsize,))
        supplier.start()
        rounds = list(map(lambda idx: cls(idx, queue, consumer), range(nproc)))
        supplier.join()
        while rounds:
            (rounds.pop(-1)).await_until_culminate()

class VerificationForecast(object):

    @staticmethod
    def _dosifize(reader, lsize):
        """
        Mechanism for avoiding usage of buffers when
        traversing huge plain textfiles.
        Args:
            reader(iterable): ref to traverse an iterable.
            lsize (int): length of lot for being hand over to a process.
        Returns:
            generator: a lot of values for being hand over to a process.
        """
        chunks = itertools.groupby(reader, lambda row: row[0])
        while True:
            # make a list of lot_size chunks
            group = [key for key, _ in itertools.islice(chunks, lsize)]
            if group:
                yield group
            else:
                break

    @classmethod
    def _serve(cls, fpath, queue, lsize):
        """
        Acts as the producer required by the pool of processes.
        Args:
            fpath (str): any file path.
            queue (Queue): a multi producer-consumer synchronized queue.
            lsize (int): length of lot for being hand over to a process.
        Returns:
            Nothing (None)
        """
        with open(fpath) as f:
            reader = csv.reader(f)
            for lot in cls._dosifize(reader, lsize):
                queue.put(lot)

    @staticmethod
    def _take(mproc_number, qprodcons, qresults, analysis_handler):
        """
        Acts as the consumer required by the pool of processes.
        Args:
            mproc_number (int): correlates process of the pool which is taking.
            qprodcons (Queue): a multi producer-consumer synchronized queue.
            qresults (Queue): a synchronized queue to place results
            analisys_handler (handler): run analisys on lot as per implementation
        Returns:
            Nothing (None)
        """
        while True:
            try:
                lot = qprodcons.get(1,1)
            except Empty:
                break
            else:
                analysis_handler(mproc_number, lot, qresults)

    @classmethod
    def _do_analysis(cls, pnum, lot, qresults, fetch_cache):
        """
        Analyzes lot of data that has been supplied via the pool
        Args:
            pnum (int): correlates process of the pool which is taking.
            lot (list): values that has been supplied through the pool.
            qresults (Queue): a synchronized queue to place results
        Returns:
            Nothing (None)
        """
        # From this point onwards we are just working with sets
        # due to they shall be our basis for comparisons.
        answer_valids, answer_invalids = fetch_cache(lot)
        lot = set(lot)

        qresults.put((pnum, (
                lot - (answer_valids | answer_invalids),  # Percolates the pending ones for verification
                lot & answer_valids,                      # Percolates those already verified and valid
                lot & answer_invalids,                    # Percolates those already verified and invalid
        )))

    @classmethod
    def _funnel(cls, fpath, lsize, fetcher, qresults):
        """
        Launches the pool of processes and
        acts as a funnel piping results in a queue.
        """
        # binding a fetcher to an analysis wrapper
        def analysis(pnum, lot, qr):
            return cls._do_analysis(pnum, lot, qr, fetcher)

        # binding the analysis of results to a consumer wrapper
        def consumer(pnum, q):
            return cls._take(pnum, q, qresults, analysis)

        # Launches the pool of processes
        _Legion.engage(fpath, lsize, cls._serve, consumer)

    @classmethod
    def render(cls, fpath, lsize, fetcher):
        """
        Renders the forecast
        """
        qresults = Queue(-1)
        fan_in = Process(target=cls._funnel, args=(fpath, lsize, fetcher, qresults,))
        fan_in.start()

        summary = [set([]), set([]), set([])]

        while True:
            try:
                pnum, percolations = qresults.get(1, 1)
            except Empty:
                break
            else:
                for idx, _ in enumerate(summary):
                    summary[idx] |= percolations[idx]

        return summary

from pymongo import MongoClient

class _MongoDBConnection():

    def __init__(self, uri):
        self._uri = uri
        self._connection = None

    def __enter__(self):
        self._connection = MongoClient(self._uri)
        return self._connection

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self._connection.close()


class MgdbFetcher(object):

    def __init__(self, uri, bot_id):
        """
        Setup the very basic variables required by the functor
        """
        self._bot_id = bot_id
        self._uri = uri

    def __call__(self, basis):
        """
        Conform the contract expected by VerificationForecast
        """
        valids, invalids = set([]), set([])

        with _MongoDBConnection(self._uri) as cli:
            db = cli.get_database('subscriptions')
            col = db.get_collection('contacts')

            question = {
                "$and": [
                    {"phone_number": {"$in": basis}},
                    {"bot_id": {"$eq": self._bot_id}},
                ]
            }

            for contact in col.find(question):
                # split up results and place them into each set
                (valids if contact["wa_id"] else invalids).add(contact["phone_number"])

        return valids, invalids


if __name__ == '__main__':
    fetcher = MgdbFetcher("mongodb://localhost:27017", "test-tech")
    a, b, c = VerificationForecast.render("../real.csv", 1000, fetcher)
    print("Pendings:{}\nValids at cache: {}\nInvalids at cache: {}".format(len(a), len(b), len(c)))
